---
title: 5-fastapi redis 配置
date: 2023-09-08 17:41:52
tags: fastapi
---

新建 `app/common_service/redis_service.py`

```py
import redis

from app.configs import conf


class RedisClient:
    def __init__(self, host: str, port: int):
        self.redis_client = redis.Redis(
            host=host, port=port, password=conf.REDIS_PASS
        )  # noqa

    def set_value(self, key: str, value: str, ex: int) -> bool:
        return self.redis_client.set(key, value, ex=ex)

    def get_value(self, key: str) -> str:
        value = self.redis_client.get(key)
        return value.decode() if value else None


redis_client = RedisClient(host=conf.INTERNAL_URL, port=conf.REDIS_PORT)

```

到这里，我们一个基本的 fastapi 就已经搭建完成。 接下来我们可以写成一个 docker-compose 方便部署
