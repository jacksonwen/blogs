---
title: fastapi初始化
date: 2023-09-07 17:15:44
tags: fastapi
---

# 安装

```
poetry add "fastapi[all]"
```

# 一个常见的项目结构

```
project/
├── app/
│   ├── api/
│   │   ├── __init__.py
│   │   ├── endpoints/
│   │   │   ├── __init__.py
│   │   │   ├── some_endpoint.py
│   │   ├── models/
│   │   │   ├── __init__.py
│   │   │   ├── some_model.py
│   ├── __init__.py
│   ├── main.py
├── tests/
│   ├── __init__.py
│   ├── test_some_endpoint.py
├── .env
├── requirements.txt
├── server.py
├── dev_server.py
├── README.md
```
