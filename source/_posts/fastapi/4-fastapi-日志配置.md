---
title: 4-fastapi 日志配置
date: 2023-09-08 16:35:59
tags: fastapi
---

日志目前可以简单配置一下即可， 打开 main.py

```py
import logging
from fastapi import FastAPI
from app.routes import api_routes

app = FastAPI()


logging.basicConfig(
    level=logging.INFO,  # 设置日志级别
    format="%(asctime)s [%(levelname)s] %(message)s",  # 设置日志格式
    datefmt="%Y-%m-%d %H:%M:%S",  # 设置日期时间格式
    handlers=[
        logging.StreamHandler(),  # 将日志消息输出到控制台
        logging.FileHandler("app.log"),  # 将日志消息写入文件
    ],
)

app.include_router(api_routes)

```
