---
title: 2-fastapi 配置文件
date: 2023-09-08 11:00:26
tags: fastapi
---
初始化配置好项目结构之后，首先要解决的是配置文件的问题， 因为不管数据库连接还是密钥管理，都要用到配置文件。


说到配置，我们要想到的有两种， 一种是应用内部的配置数据， 一种是系统的配置数据， 应用的内部数据，比如说密钥，比如说预先设置好的 admin 用户。 另外一种是系统的配置数据，比如说数据库用户密码， redis的密码等。 


这两种数据是需要分开的， 应用内的配置数据可以自己想名字，但是系统级别的配置数据，最好跟官方的一致，这点是很重要的， 一方面不会因为命名而混淆， 另一方面，在后面部署到时候，docker 可以直接读取到这些变量的值。 

开始创建配置文件
1. 新建 configs.py
```py
import os
from pydantic_settings import BaseSettings, SettingsConfigDict

# 基本配置文件字段
class BaseConf(BaseSettings):
    # 区别环境字段
    ENV: str
    # database
    POSTGRES_URL: str
    POSTGRES_USER: str
    POSTGRES_DB: str
    POSTGRES_PASSWORD: str
    # redis 
    REDIS_ARGS: str

# 指定 开发环境要使用哪种配置 
class DevConf(BaseConf):
    model_config = SettingsConfigDict(env_file=".env.dev")


class ProdConf(BaseConf):
    model_config = SettingsConfigDict(env_file=".env.prod")

# 返回一个统一的实例，后续只需要使用这个即可
conf = DevConf() if os.getenv("ENV") == "dev" else ProdConf()

```

接下来就是要配置上面代码的值, 新建 `.env.dev` 和 `.env.prod` 注意这个要放在项目根目录下，不是 app 文件夹里面
里面的字段，必须要对应上面代码的字段👆
```
ENV=dev
POSTGRES_URL=127.0.0.1
POSTGRES_USER=pguser
POSTGRES_PASSWORD=pgpass
POSTGRES_DB=pgtest
REDIS_ARGS=--requirepass qwe123
```

这样我们就配置完成了。 测试一下, 在 `main.py` 新增
```
@app.get("/index")
def show_settings():
    return {"status": conf.POSTGRES_USER}
```
然后启动服务器，输入网址，应该就能看到配置的值


# 参考
https://fastapi.tiangolo.com/advanced/settings/?h=config#the-config-file