---
title: poetry
date: 2023-09-07 17:16:30
tags: fastapi
---

# 安装

官方推荐安装方式

```
curl -sSL https://install.python-poetry.org | python3 -
```

其他方式(不推荐)

```
pip install poetry
```

# 如何使用

首先要做一个全局的配置

```
poetry config virtualenvs.in-project true
```

其次要进入操作的项目

```
mkdir <new-project>
cd <new-project>
poetry init
poetry shell
```

这样环境就配置好了，需要安装的时候只要 add 即可

```
poetry add requests
```

如果项目是第三方项目，第一次往往需要 install 而不是 add

```
cd <project>
poetry shell
poetry install
```
