---
title: 3-fastapi 数据库配置
date: 2023-09-08 16:11:05
tags: fastapi
---

# 概要

1. alembic 使用
2. sqlalchemy 的使用

# alembic

## 配置

在 `python` 里面，使用数据库是搭配使用的， 最常见的就是使用 `alembic` 进行数据库版本管理， `sqlalchemy` 做主要的数据库操作.

alembic 的主要操作十分的简单， 虽然文档很长， 这里主要记录一下经常做的一些步骤。

1. 初始化

```
 alembic init app/database/revisions/core
```

2. 修改配置文件， 打开 `app/database/revisions/core/env.py`

```py
...

config = context.config
load_dotenv(dotenv_path=f'.env.{os.getenv("ENV").lower()}')


def get_url():
    user = os.getenv("POSTGRES_USER")
    password = os.getenv("POSTGRES_PASSWORD")
    port = os.getenv("POSTGRES_PORT")
    db = os.getenv("POSTGRES_DB")
    return f"postgresql://{user}:{password}@127.0.0.1:{port}/{db}"


config.set_main_option("sqlalchemy.url", get_url())

# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
from app.database.base import Base  # noqa

target_metadata = Base.metadata


...
```

最后注释掉 `alembic.ini `

```
...
# sqlalchemy.url = driver://user:pass@localhost/dbname
...
```

这样我们数据库版本管理就配置好了。

## 使用

1. 生成数据库表

```
 alembic revision --autogenerate -m "<message>"
```

2.  迁移数据库， 一般来说，我们都需要只执行这一句

```
 alembic upgrade head
```

# SQLALCHEMY

接下来就配置一下 ORM ， 这里我们选择使用 postgres 作为后端数据库
新建 sessio.py

```py
from typing import Generator
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app.configs import conf

engine = create_engine(
    url=f"postgresql://{conf.POSTGRES_USER}:{conf.POSTGRES_PASSWORD}@127.0.0.1:{conf.POSTGRES_PORT}/{conf.POSTGRES_DB}",  # noqa
    pool_pre_ping=True,
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def get_db() -> Generator:
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()

```

新建 `base_class.py`

```py
from typing import Any
from sqlalchemy.ext.declarative import as_declarative


@as_declarative()
class Base:
    id: Any
    __name__: str
```

最后，我们要让我们的所有数据库对象被识别到， 之前我们在 👆 alembic 定义到了

```py
# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
from app.database.base_class import Base  # noqa

target_metadata = Base.metadata
```

所以我们要在 `app/database/base.py` 下面引入我们所有的对象。

```py
from app.database.base_class import Base
```

> 💡 这里用了 `base_class.py` 和 `base.py` 是有讲究的。 在我们创建的数据库对象里面，我们需要集成 `Base` 这个对象， 这个时候 `Base` 的引用必须是 `base_class.py`, 而 `base.py` 则主要是引入` model 对象`，反馈给 `alembic` ，所以不能混淆，否则容易引起循环引入从而报错

# 实践

接下来我们写个例子，来完善数据库调用。
新建

`app/api/user/models.py`

```py
from sqlalchemy import BigInteger, Column, DateTime, String
# 从base_class 引入
from app.database.base_class import Base


class User(Base):
    __tablename__ = "users"
    id = Column(BigInteger, primary_key=True, index=True)
    name = Column(String, index=True, unique=True, nullable=False)
    email = Column(String, nullable=False, index=True, unique=True)
    address = Column(String, nullable=True, index=True)
    created_at = Column(DateTime, index=True)

```

`app/api/user/schemas.py`

```py
from typing import Optional
from pydantic import BaseModel, Field


class UserCreate(BaseModel):
    name: str = Field(min_length=1, pattern=r"^[a-zA-Z0-9]*$")
    email: str = Field(pattern=r"^\w+\@\w+\.\w{3}")
    address: Optional[str] = None

```

`app/api/user/routes.py`

```py
from fastapi import APIRouter, Depends
from fastapi.encoders import jsonable_encoder
from app.api.user.models import User
from app.api.user.schemas import UserCreate

from sqlalchemy.orm import Session

from app.database.session import get_db


router = APIRouter(prefix="/users")


@router.post("")
def create(*, db: Session = Depends(get_db), request: UserCreate):
    user = User(**jsonable_encoder(request))
    db.add(user)
    db.commit()
    db.refresh(user)

```

添加到 `base.py` 反馈给 `alembic`

```py
from app.database.base_class import Base  # noqa
from app.api.user.models import User  # noqa
```

执行

```
export ENV=dev && alembic revision --autogenerate -m "add users table"
```

然后执行

```
alembic upgrade head
```

这样我们表就算建完了。

接下来引入路由,用来收集所有的路由。
新建 `app/routes.py`

```py
from fastapi import APIRouter

from app.api import user


api_routes = APIRouter()

api_routes.include_router(user.routes.router)

```

然后在 main.py

```py
from fastapi import FastAPI
from app.configs import conf
from app.routes import api_routes

app = FastAPI()


@app.get("/index")
def show_settings():
    return {"status": conf.POSTGRES_USER}


app.include_router(api_routes)

```

至此，我们就完成了 postgresql 的集成
