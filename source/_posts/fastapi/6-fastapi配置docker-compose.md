---
title: 6-fastapi配置docker-compose
date: 2023-09-08 23:05:23
tags: fastapi
---

有几个坑

1. docker 内部的网络连接不能使用 localhost ，必须使用 `host.docker.internal`
2. docker-compose 中 command 必须要用 `command: /bin/bash -c "alembic revision --autogenerate -m \"db migration\" && alembic upgrade head && uvicorn server:app --host 0.0.0.0 --port 8090"` 进行命令串联

.env.prod 文件
```
ENV=prod
INTERNAL_URL=host.docker.internal
POSTGRES_PORT=5434
POSTGRES_USER=pguser
POSTGRES_PASSWORD=pgpass
POSTGRES_DB=pgtest
REDIS_ARGS=--requirepass qwe123
REDIS_PASS=qwe123
REDIS_PORT=6381
```

Dockerfile 文件
```Dockerfile
# 设置基础镜像
FROM python:3.11

# 设置工作目录
WORKDIR /app

# 复制项目文件到容器中
COPY . /app
RUN pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple

```

docker-compose.prod.yaml
```yaml
version: "3.8"
services:
  app:
    build:
      context: .
      dockerfile: Dockerfile
    environment:
      - ENV=prod
    depends_on:
      - db
      - redis
    command: /bin/bash -c "alembic revision --autogenerate -m \"db migration\" && alembic upgrade head && uvicorn server:app --host 0.0.0.0 --port 8090"
    ports:
      - "8090:8090"
  db:
    image: postgres
    ports:
      - "127.0.0.1:5434:5432"
    env_file:
      - .env.prod
    volumes:
      - ${PWD}/data/prod/pgdata:/var/lib/postgresql/data

  redis:
    image: redis/redis-stack-server:latest
    env_file:
      - .env.prod
    ports:
      - "127.0.0.1:6381:6379"
    volumes:
      - ${PWD}/data/prod/redis-data:/data
```
deploy.sh
```sh
docker rmi $(docker images -f "dangling=true" -q)
docker-compose -f docker-compose.prod.yaml up --build
```