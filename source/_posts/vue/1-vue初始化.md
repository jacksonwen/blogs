---
title: 1-vue初始化
date: 2023-09-11 15:26:41
tags: vue3
---
目前如果是单页面的话，一般还是使用 vite 来进行初始化。 这方面就不再赘述， 记录一些个人常用的配置

`vite.conf.ts` 的内容

```ts
import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { resolve } from 'path'
import DefineOptions from 'unplugin-vue-define-options/vite' // 引入插件

export default ({ mode }) =>
  defineConfig({
    plugins: [vue(), vueJsx(), DefineOptions()],
    resolve: {
      alias: {
        '@': resolve(__dirname, 'src')
      }
    },
    css: {
      modules: {
        localsConvention: 'camelCaseOnly' // 我们使用驼峰形式
      }
    },
    server: {
      proxy: {
        '/api': {
          target: loadEnv(mode, process.cwd()).VITE_BASE_API_URL,
          changeOrigin: true,
          secure: false,
          rewrite: (path) => path.replace(/^\/api/, '')
        }
      }
    }
  })
```