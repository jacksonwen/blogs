---
title: 2-axios的封装
date: 2023-09-11 15:36:30
tags: vue3, react
---
需要安装的依赖
```sh
npm install qs -s 
npm install @types/qs -D
```

token的封装
```ts
export const TOKEN = 'TOKEN'
export const fetachToken = (): string | null => localStorage.getItem(TOKEN)
export const storeToken = (token: string): void => localStorage.setItem(TOKEN, token)
export const removeToken = () => localStorage.removeItem(TOKEN)
```

axios 的封装
```ts
import axios, { type AxiosInstance, type AxiosResponse } from 'axios'
import { TOKEN, fetachToken, removeToken } from './token'
import qs from 'qs'
import router from '@/router'

const baseURL = import.meta.env.BASE_URL
const requestTimeout = 6000

export interface HttpResponse<T = unknown> {
  status: number
  msg: string
  code: number
  data: T
}

const api: AxiosInstance = axios.create({
  baseURL,
  timeout: requestTimeout,
  headers: {
    'Content-Type': 'application/json'
  }
})

api.interceptors.request.use(
  (config) => {
    const token = fetachToken()
    if (token) {
      const token_obj = JSON.parse(token)
      config.headers['Authorization'] = `bearer ${token_obj['access_token']}`
    }
    if (
      config.data &&
      config.headers['Content-Type'] === 'application/x-www-form-urlencoded;charset=UTF-8'
    )
      config.data = qs.stringify(config.data)

    return config
  },
  (error) => Promise.reject(error)
)

api.interceptors.response.use(
  (response: AxiosResponse<HttpResponse>) => {
    const res = response
    const url = response.config.url
    if (url?.includes('download')) {
      response.headers.responseType = 'blob'
    }
    return res
  },
  (error: any) => {
    const { config } = error
    // console.error(error);
    if (error.response.status == 401) {
      const token = fetachToken()
      if (token) {
        const token_obj = JSON.parse(token)
        axios
          .post('/api/refresh-token', {
            access_token: token_obj['access_token'],
            refresh_token: token_obj['refresh_token']
          })
          .then((res) => {
            localStorage.setItem(TOKEN, JSON.stringify(res.data))
            const newAccessToken = res.data['access_token']
            config.headers.Authorization = `Bearer ${newAccessToken}`
            return axios(config)  //重新发送请求
          })
          .catch(() => {
            if (router.currentRoute.value.name != 'Login') {
              removeToken()
              router.push({ name: 'Login' })
            }
          })
      }
    }
    return Promise.reject(error)
  }
)

export default api

``