---
title: 使用llama.cpp量化部署
date: 2023-09-15 13:40:15
tags: ai
---

以llama.cpp工具为例，介绍模型量化并在本地部署的详细步骤。Windows则可能需要cmake等编译工具的安装。本地快速部署体验推荐使用经过指令精调的Alpaca-2模型，有条件的推荐使用6-bit或者8-bit模型，效果更佳。 下面以中文Alpaca-2-7B模型为例介绍，运行前请确保：

系统应有make（MacOS/Linux自带）或cmake（Windows需自行安装）编译工具
建议使用Python 3.10以上编译和运行该工具

# Step 1: 克隆和编译llama.cpp
```
$ git clone https://github.com/ggerganov/llama.cpp
```

Windows/Linux用户如需启用GPU推理，则推荐与BLAS（或cuBLAS如果有GPU）一起编译，可以提高prompt处理速度。以下是和cuBLAS一起编译的命令，适用于NVIDIA相关GPU。参考：llama.cpp#blas-build
```sh
$ make LLAMA_CUBLAS=1
```
macOS用户无需额外操作，llama.cpp已对ARM NEON做优化，并且已自动启用BLAS。M系列芯片推荐使用Metal启用GPU推理，显著提升速度。只需将编译命令改为：LLAMA_METAL=1 make，参考llama.cpp#metal-build
```sh
$ LLAMA_METAL=1 make
```
# Step 2: 生成量化版本模型
目前llama.cpp已支持.pth文件以及huggingface格式.bin的转换。将完整模型权重转换为GGML的FP16格式，生成文件路径为zh-models/7B/ggml-model-f16.gguf。进一步对FP16模型进行4-bit量化，生成量化模型文件路径为zh-models/7B/ggml-model-q4_0.gguf

这里将下载下来的百度网盘文件夹的所有东西移动到这个 zh-models/7B
```sh
$ python convert.py zh-models/7B/
$ ./quantize ./zh-models/7B/ggml-model-f16.gguf ./zh-models/7B/ggml-model-q4_0.gguf q4_0
```

# Step 3: 加载并启动模型

由于本项目推出的Alpaca-2使用了Llama-2-chat的指令模板，请首先将本项目的scripts/llama-cpp/chat.sh拷贝至llama.cpp的根目录。chat.sh文件的内容形如，内部嵌套了聊天模板和一些默认参数，可根据实际情况进行修改。

GPU推理：通过Metal编译则只需在./main中指定-ngl 1；cuBLAS编译需要指定offload层数，例如-ngl 40表示offload 40层模型参数到GPU
加载长上下文模型（16K）：
启动模型（./main）后debug信息中显示llm_load_print_meta: freq_scale     = 0.25，则表示模型转换时已载入相应超参，无需其他特殊设置
如果上述debug信息显示为llm_load_print_meta: freq_scale     = 1.0，则需在./main中额外指定--rope-scale 4

```bash
#!/bin/bash

# temporary script to chat with Chinese Alpaca-2 model
# usage: ./chat.sh alpaca2-ggml-model-path your-first-instruction

SYSTEM='You are a helpful assistant. 你是一个乐于助人的助手。'
FIRST_INSTRUCTION=$2

./main -m $1 \
--color -i -c 4096 -t 8 --temp 0.5 --top_k 40 --top_p 0.9 --repeat_penalty 1.1 \
--in-prefix-bos --in-prefix ' [INST] ' --in-suffix ' [/INST]' -p \
"[INST] <<SYS>>
$SYSTEM
<</SYS>>

$FIRST_INSTRUCTION [/INST]"
```

使用以下命令启动聊天。

```sh
$ chmod +x chat.sh
$ ./chat.sh zh-models/7B/ggml-model-q4_0.gguf '请列举5条文明乘车的建议'
```